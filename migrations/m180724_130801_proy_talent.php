<?php

use yii\db\Migration;

/**
 * Class m180724_130801_proy_talent
 */
class m180724_130801_proy_talent extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%profesion}}', [
            'profesion_id' => $this->primaryKey(),
            'nombre' => $this->string(200)->unique(),
            'activo' => $this->boolean()
        ], $tableOptions);

        $this->createTable('{{%personal}}', [
            'personal_id' => $this->primaryKey(),
            'nombre' => $this->string(200),
            'municipio_id'=>$this->integer(),
            'fecha_nacimiento'=>$this->date(),
            'correo'=>$this->string(100)->unique(),
            'profesion_id'=>$this->integer()
        ],$tableOptions);

        $this->createTable('{{%municipio}}', [
            'municipio_id' => $this->primaryKey(),
            'nombre' => $this->string(200)->unique()
        ], $tableOptions);
            
        $this->addForeignKey('FK_per_muni','municipio','municipio_id','personal','municipio_id');
        $this->addForeignKey('FK_per_pro','profesion','profesion_id','personal','profesion_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_per_muni','municipio');
        $this->dropForeignKey('FK_per_pro','profesion');
        $this->dropTable('{{%profesion}}');
        $this->dropTable('{{%personal}}');
        $this->dropTable('{{%municipio}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180724_130801_proy_talent cannot be reverted.\n";

        return false;
    }
    */
}
