<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Profesion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profesion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_profesion')->textInput(['maxlength' => true]) ?>

    <?php
    if(!$model->isNewRecord)
    echo $form->field($model, 'activo')->checkbox(); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
