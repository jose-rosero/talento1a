<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Profesion */

$this->title =  Yii::t('app','Update Profesion: ') . $model->profesion_id;
$this->params['breadcrumbs'][] = ['label' =>  Yii::t('app','Profesions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->profesion_id, 'url' => ['view', 'id' => $model->profesion_id]];
$this->params['breadcrumbs'][] =  Yii::t('app','Update');
?>
<div class="profesion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
