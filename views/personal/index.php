<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Municipio;
use app\models\Profesion;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Personals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Personal'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'personal_id',
            'nombre_personal',
            'correo',
            //'fecha_nacimiento',
            [
                'attribute'=>'municipio_id',
                'value'=>function($model){
                    return $model->municipio->nombre_municipio;
                },
                'filter'=>ArrayHelper::map(Municipio::find()->orderBy('nombre_municipio')->all(),'municipio_id','nombre_municipio')
            ],
            //'municipio.nombre_municipio',
            [
                'attribute'=>'profesion_id',
                'value'=>function($model){
                    return $model->profesion->nombre_profesion;
                },
                'filter'=>ArrayHelper::map(Profesion::find()->orderBy('nombre_profesion')->all(),'profesion_id','nombre_profesion')
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
