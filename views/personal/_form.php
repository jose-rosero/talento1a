<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Municipio;
use app\models\Profesion;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Personal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="personal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_personal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_nacimiento')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'dd-MM-yyyy',
        'value' => date('d/m/Y'),
        'options' => ['placeholder' => '01-01-0001','style' => 'position: relative; z-index: 999', 'class' => 'form-control']
    ]) ?>

    <?php
    $municipios = ArrayHelper::map(Municipio::find()->orderBy('nombre_municipio')->all(),'municipio_id','nombre_municipio');
    echo $form->field($model, 'municipio_id')->widget(Select2::classname(), [
        'data' => $municipios,
        'language' => 'es',
        'options' => ['placeholder' => 'Seleccione un municipio ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php
    $profesion = ArrayHelper::map(Profesion::find()->where(['activo'=>1])->orderBy('nombre_profesion')->all(),'profesion_id','nombre_profesion');
    echo $form->field($model, 'profesion_id')->widget(Select2::classname(), [
        'data' => $profesion,
        'language' => 'es',
        'options' => ['placeholder' => 'Seleccione un proyecto ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
