<?php

namespace app\controllers;
use \yii\rest\ActiveController;
use \yii\helpers\ArrayHelper;
use \yii\filters\auth\HttpBasicAuth;
use \yii\filters\AccessControl;


class ApipsController extends ActiveController {

    public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public $modelClass = 'app\models\Personal';

    public function behaviors() {
        return ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'class' => HttpBasicAuth::className(),
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'delete', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }
    public function actionIndex(){
        $query = \app\models\Profesion::find();
        $query->andWhere("activo='true'");
        return $query->all();
    }
}