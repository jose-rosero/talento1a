<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profesion".
 *
 * @property int $profesion_id
 * @property string $nombre
 * @property bool $activo
 *
 * @property Personal[] $personals
 */
class Profesion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profesion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo'], 'boolean'],
            [['nombre'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'profesion_id' => Yii::t('app', 'Profesion ID'),
            'nombre_profesion' => Yii::t('app', 'Nombre Profesion'),
            'activo' => Yii::t('app', 'Activo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonals()
    {
        return $this->hasMany(Personal::className(), ['profesion_id' => 'profesion_id']);
    }

    public function beforeSave($insert){
        parent::beforeSave($insert);
        if($insert)
            $this->activo = 1;
        return true;
    }
}
