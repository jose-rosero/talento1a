<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personal".
 *
 * @property int $personal_id
 * @property string $nombre_personal
 * @property string $correo
 * @property string $fecha_nacimiento
 * @property int $municipio_id
 * @property int $profesion_id
 *
 * @property Municipio $municipio
 * @property Profesion $profesion
 */
class Personal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento'], 'safe'],
            [['municipio_id', 'profesion_id'], 'default', 'value' => null],
            [['municipio_id', 'profesion_id'], 'integer'],
            [['nombre_personal', 'correo'], 'string', 'max' => 200],
            [['municipio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Municipio::className(), 'targetAttribute' => ['municipio_id' => 'municipio_id']],
            [['profesion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profesion::className(), 'targetAttribute' => ['profesion_id' => 'profesion_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'personal_id' => Yii::t('app', 'Personal ID'),
            'nombre_personal' => Yii::t('app', 'Nombre Personal'),
            'correo' => Yii::t('app', 'Correo'),
            'fecha_nacimiento' => Yii::t('app', 'Fecha Nacimiento'),
            'municipio_id' => Yii::t('app', 'Municipio ID'),
            'profesion_id' => Yii::t('app', 'Profesion ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipio()
    {
        return $this->hasOne(Municipio::className(), ['municipio_id' => 'municipio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesion()
    {
        return $this->hasOne(Profesion::className(), ['profesion_id' => 'profesion_id']);
    }
}
