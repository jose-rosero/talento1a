<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Profesion;

/**
 * ProfesionSearch represents the model behind the search form of `app\models\Profesion`.
 */
class ProfesionSearch extends Profesion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profesion_id'], 'integer'],
            [['nombre_profesion'], 'safe'],
            [['activo'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Profesion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'profesion_id' => $this->profesion_id,
            'activo' => $this->activo,
        ]);

        $query->andFilterWhere(['ilike', 'nombre_profesion', $this->nombre_profesion]);

        return $dataProvider;
    }
}
